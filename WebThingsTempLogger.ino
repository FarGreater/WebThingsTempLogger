/**
 * ****************************************************************************
 * 
 * * WebThingsTempLogger
 * 
 * ****************************************************************************
 * By Michael Egtved Christensen 2019 ->
 *
 *  
 * see https://github.com/esp8266/Arduino for setting up the Arduino enviroment
 * 
 *
 * 
 * * Fremhæv
 * ! Advarsel
 * ? Spørgsmål
 * TODO: TODO
 * @param parameter
*/

#include <OneWire.h>

#include <MecLogging.h>
#include <MecWifi.h>
#include <MecWebThing.h>

/**
 * ****************************************************************************
 *  GLOBALS
 * ****************************************************************************
 */

int OwPin = 12;
int interval = 5000;
int temptimer = 0;
OneWire OwTemp(OwPin); // on pin 2 (a 4.7K resistor is necessary)
int sensorcount = 0;

/**
 * ****************************************************************************
 *  Setting up Debug
 * ****************************************************************************
 */

// LED to flash when there is activity
#define ActivityLedPin D4
#define ActivityLedMode Normal //Inverse // None, Inverse or Normal
// Debug level
//    - Inf for "production"
//    - Deb for "debug/development"
//    see libary Debug.h for all the debug levels
#define MyLogLevel Inf

MecLogging MyLog(MyLogLevel, ActivityLedPin, ActivityLedMode);

/**
 * ****************************************************************************ar
 *  Setting up Wifi
 * ****************************************************************************
 */
String WiFiSSID[] = {"Clemens 5n",
                     "Clemens 5nn",
                     "Michaels Phone",
                     "Clemens-5G"};
String WiFiPassword[] = {"zurtkeld",
                         "zurtkeld",
                         "zurtkeld",
                         "zurtkeld"};
int WifiCount = 4;

String ThingID = "TempLogger";

MecWifi MyWifi(WiFiSSID, WiFiPassword, WifiCount, &MyLog, ThingID);

/**
 * ****************************************************************************
 *  Setting up Wifi
 * ****************************************************************************
 */

String ThingType[] = {"TemperatureSensor"};
int ThingTypeCount = 1;

MecThing Thing(ThingID, "TempLogger", "Temperatur logger frysere", ThingType, ThingTypeCount, &MyLog, &MyWifi);

float DefaultValue = 99.99;
MecProperty TemperaturePropertyFryserET(&MyLog, "TemperaturePropertyFryserET", "Fryser 1 (Højre)", "Temperaturen Fryser 1 (Højre)", DefaultValue, "TemperatureProperty", true);
MecProperty TemperaturePropertyFryserTO(&MyLog, "TemperaturePropertyFryserTO", "Fryser 2 (Venstre)", "Temperaturen Fryser 2 (Venstre)", DefaultValue, "TemperatureProperty", true);
MecProperty TemperaturePropertyViktualie(&MyLog, "TemperaturePropertyViktualie", "Viktualierum", "Temperaturen i Viktualierum", DefaultValue, "TemperatureProperty", true);
MecProperty TemperaturePropertyGrovKoekken(&MyLog, "TemperaturePropertyGrovKoekken", "Grovkøkkenet", "Temperaturen Grovkøkkenet", DefaultValue, "TemperatureProperty", true);

/**
 * ****************************************************************************
 * ****************************************************************************
 *  setup
 * ****************************************************************************
 */
void setup()
{
  // Init libaries
  MyLog.ActivityLED(LedON);
  MyLog.setup();
  MyWifi.setup();

  Thing.addProperty(&TemperaturePropertyFryserET);
  Thing.addProperty(&TemperaturePropertyFryserTO);
  Thing.addProperty(&TemperaturePropertyViktualie);
  Thing.addProperty(&TemperaturePropertyGrovKoekken);

  Thing.setup();

  MyLog.infoNl("Setup- start...", Deb);

  // publish end setup
  MyLog.infoNl("Setup- End...", Deb);
  MyLog.ActivityLED(LedOFF);
}

///////////////////////////////////////////////////////////////////////////////
// ReadAndSendOWSensorData
//
////////////////////////////////////////////////////////////////////////////////

void ReadAndSendOWSensorData()
{
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  int moredata;
  String address;
  String sensordata;
  int tmpsensorcount = 0;
  float temp;
  int itemp;

  // Genstart søgning efter OW enheder
  OwTemp.reset_search();

  while (true)
  {
    // find next sensor adress
    moredata = OwTemp.search(addr);
    // Break if no more adresses(sensors) found
    if (!moredata)
    {
      MyLog.infoNl("Loop-OW- No more addresses.", Deb);
      OwTemp.reset_search();
      break;
    }
    tmpsensorcount++;
    // check adress is valid, skip to next sensor if CRC not OK
    if (OneWire::crc8(addr, 7) != addr[7])
    {
      MyLog.infoNl("Loop-OW- CRC is not valid!", Deb);
      continue;
    };
    // Convert address to hex tekst string
    address = "";
    for (i = 0; i < 8; i++)
    {
      address = address + String(addr[i], HEX);
    }

    // read values
    OwTemp.reset();
    OwTemp.select(addr);
    OwTemp.write(0x44, 1); // start conversion, with parasite power on at the end

    for (i = 0; i < 9; i++)
    {
      MyLog.ActivityLED(LedActivity);
      delay(100);
    }
    MyLog.ActivityLED(LedActivity);

    //delay(1000);     // maybe 750ms is enough, maybe not
    // we might do a ds.depower() here, but the reset will take care of it.

    present = OwTemp.reset();
    OwTemp.select(addr);
    OwTemp.write(0xBE); // Read Scratchpad

    sensordata = "";
    for (i = 0; i < 9; i++)
    { // we need 9 bytes
      data[i] = OwTemp.read();
      sensordata = sensordata + String(data[i], HEX);
    }

    // check adress is valid, skip to next sensor if CRC not OK
    if (OneWire::crc8(data, 8) != data[8])
    {
      MyLog.infoNl("Loop-OW- Payload CRC is not valid! adress =0x" + address + " CRC =" + String(OneWire::crc8(addr, 7), HEX), Deb);
      continue;
    };

    // test data -0.5
    //    data[1] = 255;
    //    data[0] = 248;

    // test data -25,0625
    //   data[1] = 254;
    //   data[0] = 111;

    if (data[1] < 128)
    {
      //MyLog.infoNl("Loop-OW- positive tal", Deb);
      temp = ((data[1] << 8) + data[0]) * 0.0625;
    }
    else
    {
      //MyLog.infoNl("Loop-OW- nagative tal", Deb);
      int tmax = 32768;
      temp = 0;
      itemp = (data[1] << 8) + data[0] - 1;

      for (int n = 0; n < 16; n++)
      {
        temp = temp * 2;
        if (itemp >= tmax)
        {
          itemp = itemp - tmax;
        }
        else
        {
          temp = temp + 1;
        }
        tmax = tmax / 2;

        //MyLog.infoNl("Loop-OW- itemp=" + String(itemp, BIN)
        //             + "temp=" + String(temp, BIN)
        //             + "tmax=" + String(tmax, BIN)
        //             , Deb);
      }
      temp = temp * -1 / 16;
    }

    MyLog.infoNl("Loop-OW- adress=0x" + address + " CRC=" + String(OneWire::crc8(addr, 7), HEX) + " P=" + String(present, HEX) + " payload=" + sensordata + " CRC=" + String(OneWire::crc8(data, 8)) + " Sensor data=" + temp, Inf);

    if (address == "28fdf439400da")
    {
      TemperaturePropertyViktualie.SetValue(temp);
    }
    if (address == "285f173a400f4")
    {
      TemperaturePropertyGrovKoekken.SetValue(temp);
    }
    if (address == "28e1bc1e0080cf")
    {
      TemperaturePropertyFryserET.SetValue(temp);
    }
    if (address == "2845ec1e0080da")
    {
      TemperaturePropertyFryserTO.SetValue(temp);
    }
  }

  if (sensorcount == 0)
  {
    sensorcount = tmpsensorcount;
  }

  if (!(tmpsensorcount == sensorcount))
  {
    //  MyLog.infoNl("Loop-OW Sensorcont mitchmatch sensors=" + sensorcount + " thisloopcount=" + tmpsensorcount , War);
    MyLog.infoNl("Loop-OW- Sensorcont mitchmatch sensors=", War);
  }
  sensorcount = tmpsensorcount;
}

/**
 * ****************************************************************************
 * ****************************************************************************
 *  loop
 * ****************************************************************************
 */
void loop()
{
  // Loop libaries
  MyLog.ActivityLED(LedON);
  MyWifi.loop();
  MyLog.loop();
  Thing.loop();

  // put your main code here, to run repeatedly:

  if (temptimer < millis())
  {
    //MyLog.infoNl("Loop- interval-start", Inf);
    temptimer = millis() + interval;
    ReadAndSendOWSensorData();
  }

  // put your main code here, to run repeatedly:

  // publish end loop
  MyLog.ActivityLED(LedOFF);
}
